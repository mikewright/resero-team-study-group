Compiling Typescript
===========================================

In today's study group we are going to get used to typescript's compiler and the configuration
options that we can provide with it.  

## `tsc`

For typescript, the command line option for compiling/transpiling from typescript to javascript is `tsc`.  

The compiler is simple and can be used to take a `.ts` file and output a corresponding `.js` file
for the given input.  

Lets first try it out by just generating a js file for the `index.ts` file under `01-tsc`.  

    tsc 01-tsc/index.ts
    node 01-tsc/index.js

This is pretty simple and we can see that it will create an `index.js` file as a sibling to the
`index.ts` file.  

### Multiple Files

While it is nice to be able to handle one file, it would be better if we could handle multiple files
at the same time.  We could accomplish this by having a `Makefile` that has a rule for `*.ts` files,
but luckily the `tsc` command provides the ability to work with multiple files already.  

Lets test this out by compiling the files in the `01-tsc/multiple` directory.  

    tsc 01-tsc/multiple/*.ts
    node 01-tsc/multiple/main.js

So, when we provide a `*.ts` it means it will resolve all the files that have the `.ts` extension with
the supplied path.  

Lets try it on a different path, one that has children paths with files that are referenced.  

    tsc 01-tsc/deep/*.ts

Hmm, it looks like this fails because it is unable to find the module that it needs.  Namely, it is
unable to import `.src/name`.  This is because the `name.ts` file lives in a different directory.  To
resolve this, lets first try a path expansion technique for linux systems `**/*.ts`. 

    tsc 01-tsc/deep/**.ts
    cd 01-tsc/deep
    node index.js

### Other options

_--outDir_  

Keep the output files in a different directory (to not pollute your src directory).  Can, and should, be
used in conjuction with `--rootDir`  

_--outFile_  

`tsc` has a few other useful options that we will want to look at.  First, we have the ability to take
all the results and dump them to a single file using `--outFile`.  However it is important to note, that
you must provide a `--module` option when using `--outFile`.  

_--module_  

Specify module code generation: "None", "CommonJS", "AMD", "System", "UMD", "ES6", "ES2015" or "ESNext". When
used with `--outFile` it must be either `AMD` or `System`.  

_--target_  

This allows you to specify the ECMAScript version to output.  

_--watch_  

Watch for file changes.  

_--noImplicitThis_  

If there are any locations where `this` could be `any` it will be an error.  

_--listFiles_ and _--listEmittedFiles_  

Outputs the files it is compling and the files emitted while compiling.  

_--lib_  

Libraries to include in the compilation.  

_--incremental_  

Keep build information for faster compilation.  

_--declaration_  

Used to generate declaration files with output, as `.d.ts` files.  

_--baseUrl_  

Base directory to resolve non-relative module names.  

_--init_  

Create a `.tsconfig` file.  

## `tsconfig.json`

The `tsconfig.json` file is a file that defines a typescript project, it is just like the `package.json` file of
a `node.js` project.  

The `tsconfig.json` file is the recommended way for working with `tsc` such that any call to `tsc` without any
options or arguments will result in it reading the `tsconfig.json` file of the local directory.  

The structure of the config file is pretty simple.  

    {
        "compilerOptions": {
            "<key>": "<value>",
        },
        "files": [
          ...
        ]
    }

There are two main sections, `compilerOptions` and `files`.  The `files` is just a list of the files that should
be compiled by `tsc` and the `compilerOptions` are the options to use when compiling.  

If the `files` idea can be to detailed, you could use the `include` and `exclude` sections instead.  These allow
you to specify a list of expressions instead of a list of files to be either ignored by the compiler or included
in the compilation process.  

There are a few other sections as well:

- `compileOnSave`
- `extends`
- `references`
- `typeAcquisition`

Let's look at the `tsconfig.json` file that was setup for this workshop and also run a build.  

### `extends`

There is another useful feature of `tsconfig.json` and that is the `extends` options.  This option allows you to
create a common base config and allow other configs to consume.  A good structure could be.  

    tsconfig.base.json
    {
      "compilerOptions": {
        "noImplicitThis": true
      },
      "include": [
        "src/**.ts"
      ]
    }

    tsconfig.dev.json
    {
      "extends": "tsconfig.base.json",

      "compilerOptions": {
        "strictNullChecks": false,
        "incremental": true,
        "watch": true,
      }
    }

    tsconfig.json
    {
      "extends": "tsconfig.base.json",

      "compilerOptions": {
        "declaration": true,
        "target": "ES6"
      }
    }
