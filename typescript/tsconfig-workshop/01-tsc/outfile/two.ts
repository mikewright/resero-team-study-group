class Second {
  num: number;

  constructor(num: number = 10) {
    this.num = num;
  }
}

export { Second as default };
