class Name {
  name: string;

  constructor(name: string) {
    this.name = name;
  }
}

export { Name as default };
