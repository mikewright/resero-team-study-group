class First {
  name: String;

  constructor(name: string) {
    this.name = name;
  }
}

export { First as default };
